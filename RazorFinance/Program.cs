﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RazorFinance
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer1 = new Stopwatch();
            timer1.Start();
            var syncTask = new SynchronousTaskRazorFinance(name: "Synchronously");
            syncTask.CollectUserDetails();
            Console.WriteLine($"Synchronous time taken: {timer1.ElapsedMilliseconds} milliseconds");
        }
    }
}
