﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RazorFinance
{
    class SynchronousTaskRazorFinance
    {
        private readonly string _name;

        public SynchronousTaskRazorFinance( string name)
        {
            _name = name;
        }
        public void CollectUserDetails()
        {
            Console.WriteLine($"Task is performed {_name}");
            Console.WriteLine("Please input your username and password");
            Application.ConfirmUserExists();
        }
    }
}
