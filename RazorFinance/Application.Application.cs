﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinance
{
    internal partial class Application
    {
        public static async Task ConfirmUserExistsAsync()
        {
            Console.WriteLine("Query the database for this user");

            Task.Delay(3000).Wait();
            Console.WriteLine("Does the query return a user?");
            Console.WriteLine("1. Yes \n2. No");
            UserDeetsAsync();
            //var answer = Console.ReadLine();
            Console.WriteLine("User Exists");

            Console.WriteLine("Querying second DB");
            Task.Delay(3000).Wait();

            QuerySecondDB();
        }

        private static async Task<User> UserDeetsAsync()
        {
            Console.WriteLine("Checking database for this username");
            Task.Delay(3000).Wait();
            Console.WriteLine("Database Query returns a user");

            return new User(); // if a class is not static, it should return something.
        }
    }
}
