﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinance
{
    internal static partial class Application
    {
        public static void ConfirmUserExists()
        {
            Console.WriteLine("Query the database for this user");

            Task.Delay(3000).Wait();
            Console.WriteLine("Does the query return a user?");
            Console.WriteLine("1. Yes \n2. No");
            UserDeets();
            //var answer = Console.ReadLine();
            Console.WriteLine("User Exists");

            Console.WriteLine("Querying second DB");
            Task.Delay(3000).Wait();

            QuerySecondDB();
            
            //if (answer == "1")
            //{
            //    QuerySecondDB();
            //}
            //if (answer == "2")
            //{
            //    Console.WriteLine("This user does not exist");
            //}
        }

        public static void QuerySecondDB()
        {
            Console.WriteLine("Querying DB for user details");
            Task.Delay(3000).Wait();
            Console.WriteLine("Your Account Number is: ACCOUNT NUMBER");
            Task.Delay(300).Wait();
            Console.WriteLine(" Your Email is: EMAIL");
            Task.Delay(300).Wait();
            Console.WriteLine("Your Username is: USERNAME");
            Task.Delay(300).Wait();
            Console.WriteLine("Your address is: ADDRESS");
        }
        private static User UserDeets()
        {
            Console.WriteLine("Checking database for this username");
            Task.Delay(3000).Wait();
            Console.WriteLine("Database Query returns a user");

            return new User(); // if a class is not static, it should return something.
        }
    }
}
